using Microsoft.VisualStudio.TestTools.UnitTesting;
using Net.Movies.Domain.Entities;

namespace Net.Movies.Tests.Entities
{
    [TestClass]
    public class AdminUserTest
    {
        [TestMethod]
        public void ShouldReturnInvalidWhenEmailIsInvalid()
        {
            var adminUser = new AdminUser("dummy", "dummy@gmail", "dummyPassword");

            Assert.IsTrue(adminUser.Valid);
        }

        [TestMethod]
        public void ShouldReturnValidWhenEmailIsValid()
        {
            var adminUser = new AdminUser("dummy", "dummy@gmail.com", "dummyPassword");

            Assert.IsTrue(adminUser.Valid);
        }
    }
}
