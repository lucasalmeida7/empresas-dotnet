FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

# Restore
FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["Net.Movies.Api/Net.Movies.Api.csproj", "Net.Movies.Api/"]
RUN dotnet restore "Net.Movies.Api/Net.Movies.Api.csproj"

# Build
COPY . .
WORKDIR "/src/Net.Movies.Api"
RUN dotnet build "Net.Movies.Api.csproj" -c Release -o /app/build

# Publish
FROM build AS publish
RUN dotnet publish "Net.Movies.Api.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Net.Movies.Api.dll"]