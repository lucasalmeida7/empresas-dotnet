﻿using Microsoft.Extensions.DependencyInjection;
using Net.Movies.Application.AppServices;
using Net.Movies.Application.Contracts;
using Net.Movies.Domain.Contracts;
using Net.Movies.Domain.Repositories;
using Net.Movies.Domain.Services;
using Net.Movies.Infra.Data.Repositories;

namespace Net.Movies.CrossCutting
{
    public static class Injector
    {
        public static void AddInjection(this IServiceCollection services)
        {
            #region AppServices

            services.AddScoped<IAdminUserAppService, AdminUserAppService>();
            services.AddScoped<ICommonUserAppService, CommonUserAppService>();
            services.AddScoped<IAccountAppService, AccountAppService>();
            services.AddScoped<IMovieAppService, MovieAppService>();
            services.AddScoped<IMovieRatingAppService, MovieRatingAppService>();
            services.AddScoped<IGenderAppService, GenderAppService>();
            services.AddScoped<IActorAppService, ActorAppService>();

            #endregion

            #region Service

            services.AddScoped<IAdminUserService, AdminUserService>();
            services.AddScoped<ICommonUserService, CommonUserService>();
            services.AddScoped<IMovieService, MovieService>();
            services.AddScoped<IMovieActorService, MovieActorService>();
            services.AddScoped<IMovieGenderService, MovieGenderService>();
            services.AddScoped<IMovieRatingService, MovieRatingService>();
            services.AddScoped<IGenderService, GenderService>();
            services.AddScoped<IActorService, ActorService>();

            #endregion

            #region Repositories

            services.AddScoped<IAdminUserRepository, AdminUserRepository>();
            services.AddScoped<ICommonUserRepository, CommonUserRepository>();

            services.AddScoped<IActorRepository, ActorRepository>();
            services.AddScoped<IDirectorRepository, DirectorRepository>();
            services.AddScoped<IGenderRepository, GenderRepository>();

            services.AddScoped<IMovieRepository, MovieRepository>();
            services.AddScoped<IMovieActorRepository, MovieActorRepository>();
            services.AddScoped<IMovieGenderRepository, MovieGenderRepository>();
            services.AddScoped<IMovieRatingRepository, MovieRatingRepository>();

            #endregion
        }
    }
}
