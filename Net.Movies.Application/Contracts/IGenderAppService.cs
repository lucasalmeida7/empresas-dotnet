﻿
using Net.Movies.Application.Comunication;
using Net.Movies.Application.ViewModels.Gender;

namespace Net.Movies.Application.Contracts
{
    public interface IGenderAppService
    {
        AppServiceResponse Insert(GenderInputViewModel input);
    }
}
