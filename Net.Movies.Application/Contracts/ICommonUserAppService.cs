﻿
using Net.Movies.Application.Comunication;
using Net.Movies.Application.ViewModels.CommonUser;
using Net.Movies.Application.ViewModels.Pagination;
using System;

namespace Net.Movies.Application.Contracts
{
    public interface ICommonUserAppService
    {
        AppServiceResponse Delete(Guid id);
        AppServiceResponse Insert(CommonUserInputViewModel input);
        AppServiceResponse Select(PaginationInputViewModel filter);
        AppServiceResponse Update(Guid id, CommonUserInputViewModel input);
    }
}
