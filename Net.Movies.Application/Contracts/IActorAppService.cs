﻿using Net.Movies.Application.Comunication;
using Net.Movies.Application.ViewModels.Actor;

namespace Net.Movies.Application.Contracts
{
    public interface IActorAppService
    {
        AppServiceResponse Insert(ActorInputViewModel input);
    }
}
