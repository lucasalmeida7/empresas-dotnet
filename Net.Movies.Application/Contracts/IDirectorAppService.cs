﻿
using Net.Movies.Application.Comunication;
using Net.Movies.Application.ViewModels.Director;

namespace Net.Movies.Application.Contracts
{
    public interface IDirectorAppService
    {
        AppServiceResponse Insert(DirectorInputViewModel input);
    }
}
