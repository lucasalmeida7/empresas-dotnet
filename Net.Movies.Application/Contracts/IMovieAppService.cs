﻿using Net.Movies.Application.Comunication;
using Net.Movies.Application.ViewModels.Movie;
using Net.Movies.Application.ViewModels.Pagination;
using System;
using System.Collections.Generic;
using System.Text;

namespace Net.Movies.Application.Contracts
{
    public interface IMovieAppService
    {
        AppServiceResponse Insert(MovieInputViewModel input);
        AppServiceResponse Select(Guid id);
        AppServiceResponse Select(PaginationInputViewModel filter);
    }
}
