﻿
using Net.Movies.Application.Comunication;
using Net.Movies.Application.ViewModels;
using Net.Movies.Application.ViewModels.AdminUser;
using System;

namespace Net.Movies.Application.Contracts
{
    public interface IAdminUserAppService
    {
        AppServiceResponse Delete(Guid id);
        AppServiceResponse Insert(AdminUserInputViewModel input);
        AppServiceResponse Update(Guid id, AdminUserInputViewModel input);
    }
}
