﻿
using Net.Movies.Application.Comunication;
using Net.Movies.Application.ViewModels.Account;

namespace Net.Movies.Application.Contracts
{
    public interface IAccountAppService
    {
        AppServiceResponse Login(LoginInputViewModel input);
    }
}
