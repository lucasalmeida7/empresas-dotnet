﻿
using Net.Movies.Application.Comunication;
using Net.Movies.Application.ViewModels.MovieRating;

namespace Net.Movies.Application.Contracts
{
    public interface IMovieRatingAppService
    {
        AppServiceResponse Insert(MovieRatingInputViewModel input);
    }
}
