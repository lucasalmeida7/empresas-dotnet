﻿using AutoMapper;
using Net.Movies.Application.ViewModels.Actor;
using Net.Movies.Application.ViewModels.AdminUser;
using Net.Movies.Application.ViewModels.Account;
using Net.Movies.Application.ViewModels.CommonUser;
using Net.Movies.Application.ViewModels.Director;
using Net.Movies.Application.ViewModels.Gender;
using Net.Movies.Application.ViewModels.Movie;
using Net.Movies.Application.ViewModels.MovieRating;
using Net.Movies.Domain.Entities;
using System.Linq;

namespace Net.Movies.Application.Automapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            #region AdminUser

            CreateMap<AdminUserInputViewModel, AdminUser>();
            CreateMap<AdminUser, AdminUserOutputViewModel>();
            CreateMap<AdminUser, LoginOutputViewModel>();

            #endregion

            #region CommonUser

            CreateMap<CommonUserInputViewModel, CommonUser>();
            CreateMap<CommonUser, CommonUserOutputViewModel>();
            CreateMap<CommonUser, LoginOutputViewModel>();

            #endregion

            #region Movie

            CreateMap<MovieInputViewModel, Movie>();
            CreateMap<Movie, MovieInfoOutputViewModel>()
                .ForMember(dest => dest.MeanScore, opt => opt.MapFrom(src => src.MovieRatings.Average(x => x.Score)));
            CreateMap<Movie, MovieOutputViewModel>()
                .ForMember(dest => dest.Director, opt => opt.MapFrom(src => src.Director.Name))
                .ForMember(dest => dest.MeanScore, opt => opt.MapFrom(src => src.MovieRatings.Average(x => x.Score)));

            #endregion

            #region MovieRating

            CreateMap<MovieRatingInputViewModel, MovieRating>();

            #endregion

            #region Director

            CreateMap<Director, DirectorOutputViewModel>();

            #endregion

            #region Actor

            CreateMap<Actor, ActorOutputViewModel>();

            #endregion

            #region Gender

            CreateMap<GenderInputViewModel, Gender>();
            CreateMap<Gender, GenderOutputViewModel>();

            #endregion
        }
    }
}
