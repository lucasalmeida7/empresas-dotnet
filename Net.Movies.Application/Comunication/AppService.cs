﻿using System.Collections.Generic;

namespace Net.Movies.Application.Comunication
{
    public class AppService
    {
        public AppServiceResponse Ok()
        {
            return new AppServiceResponse()
            {
                Result = new object(),
                StatusCode = 200,
                Messages = new List<string>(),
            };
        }

        public AppServiceResponse Ok(object obj, params string[] messages)
        {
            return new AppServiceResponse()
            {
                Result = obj,
                StatusCode = 200,
                Messages = messages,
            };
        }

        public AppServiceResponse Fail(params string[] messages)
        {
            return new AppServiceResponse()
            {
                Result = null,
                StatusCode = 400,
                Messages = messages,
            };
        }
    }
}
