﻿
using System.Collections.Generic;

namespace Net.Movies.Application.Comunication
{
    public class AppServiceResponse
    {
        public virtual object Result { get; set; }
        public virtual int StatusCode { get; set; }
        public virtual IEnumerable<string> Messages { get; set; }
    }
}
