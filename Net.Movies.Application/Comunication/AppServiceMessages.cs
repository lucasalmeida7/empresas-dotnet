﻿
namespace Net.Movies.Application.Comunication
{
    public static class AppServiceMessages
    {
        public const string REPONSE_IS_NULL_MESSAGE = "Ocorreu um erro ao realizar a operação";

        public static class ViewModel
        {
            public const string REQUIRED_MESSAGE = "O campo {0} é obrigatório";
        }
    }
}
