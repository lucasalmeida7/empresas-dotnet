﻿using Net.Movies.Domain.Entities;
using System;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;

namespace Net.Movies.Application.Pagination
{
    public class QueryPagination<T> where T : class
    {
        private IQueryable<T> _queryable;
        private int _take;
        private int _skip;
        private bool _ascending;
        private Expression<Func<T, object>> _expression;

        public QueryPagination(IQueryable<T> queryable, int take, int page, bool ascending, Expression<Func<T, object>> OrderByargument)
        {
            _queryable = queryable;
            _take = take;
            _skip = page;
            _ascending = ascending;
            _expression = OrderByargument;
        }

        public IQueryable<T> Filter()
        {
                _queryable =
                    _ascending 
                    ? _queryable.OrderBy(_expression) 
                    : _queryable.OrderByDescending(_expression);

            return _queryable.Skip(_take * (_skip - 1)).Take(_take);
        }

    }
}
