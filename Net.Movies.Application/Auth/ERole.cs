﻿
namespace Net.Movies.Application.Auth
{
    public enum ERole
    {
        AdminUser = 0,
        CommonUser = 1
    }
}
