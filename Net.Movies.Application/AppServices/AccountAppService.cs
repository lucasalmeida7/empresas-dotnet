﻿
using AutoMapper;
using Net.Movies.Api.Auth;
using Net.Movies.Application.Auth;
using Net.Movies.Application.Comunication;
using Net.Movies.Application.Contracts;
using Net.Movies.Application.ViewModels.Account;
using Net.Movies.Domain.Contracts;
using System.Linq;

namespace Net.Movies.Application.AppServices
{
    public class AccountAppService : AppService, IAccountAppService
    {
        protected readonly IMapper _mapper;
        protected readonly IAdminUserService _adminService;
        protected readonly ICommonUserService _commonService;

        public AccountAppService(IMapper mapper, IAdminUserService adminService, ICommonUserService commonService)
        {
            _mapper = mapper;
           _adminService = adminService;
           _commonService = commonService;
        }

        public AppServiceResponse Login(LoginInputViewModel input)
        {
            var responseAdmin = _adminService.Login(input.Email, input.Password);

            var responseCommon = _commonService.Login(input.Email, input.Password);

            if (responseAdmin == null || responseCommon == null)
                return Fail(AppServiceMessages.REPONSE_IS_NULL_MESSAGE);
            
            if (responseAdmin.Messages.Any() && responseCommon.Messages.Any())
                return Fail(responseAdmin.Messages.ToArray());

            var token = "";
            var result = new LoginOutputViewModel();

            if (responseAdmin.Result != null)
            {
                token = TokenService.GenerateToken(responseAdmin.Result, ERole.AdminUser);
                result = _mapper.Map<LoginOutputViewModel>(responseAdmin.Result);
                result.Token = token;
            }

            if (responseCommon.Result != null)
            {
                token = TokenService.GenerateToken(responseCommon.Result, ERole.CommonUser);
                result = _mapper.Map<LoginOutputViewModel>(responseCommon.Result);
                result.Token = token;
            }

            return Ok(result);
        }
    }
}
