﻿using AutoMapper;
using Net.Movies.Application.Comunication;
using Net.Movies.Application.Contracts;
using Net.Movies.Application.ViewModels.Gender;
using Net.Movies.Domain.Contracts;
using Net.Movies.Domain.Entities;
using System.Linq;

namespace Net.Movies.Application.AppServices
{
    public class GenderAppService : AppService, IGenderAppService
    {
        private readonly IMapper _mapper;
        private readonly IGenderService _service;

        public GenderAppService(IMapper mapper, IGenderService service)
        {
            _mapper = mapper;
            _service = service;
        }

        public AppServiceResponse Insert(GenderInputViewModel input)
        {
            var model = _mapper.Map<Gender>(input);

            var response = _service.Add(model);

            if (response.Messages.Any())
                return Fail(response.Messages.ToArray());

            return Ok(response.Result);
        }
    }
}
