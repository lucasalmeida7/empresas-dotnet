﻿using AutoMapper;
using Net.Movies.Application.Comunication;
using Net.Movies.Application.Contracts;
using Net.Movies.Application.ViewModels.MovieRating;
using Net.Movies.Domain.Contracts;
using Net.Movies.Domain.Entities;
using System.Linq;

namespace Net.Movies.Application.AppServices
{
    public class MovieRatingAppService : AppService, IMovieRatingAppService
    {
        protected readonly IMapper _mapper;
        protected readonly IMovieRatingService _service;

        public MovieRatingAppService(IMapper mapper, IMovieRatingService service)
        {
            _mapper = mapper;
            _service = service;
        }

        public AppServiceResponse Insert(MovieRatingInputViewModel input)
        {
            var model = _mapper.Map<MovieRating>(input);

            var response = _service.Add(model);

            if (response == null)
                return Fail(AppServiceMessages.REPONSE_IS_NULL_MESSAGE);

            if (response.Messages.Any())
                return Fail(response.Messages.ToArray());

            return Ok(response.Result);
        }
    }
}
