﻿using AutoMapper;
using Net.Movies.Application.Comunication;
using Net.Movies.Application.Contracts;
using Net.Movies.Application.Pagination;
using Net.Movies.Application.ViewModels.CommonUser;
using Net.Movies.Application.ViewModels.Pagination;
using Net.Movies.Domain.Contracts;
using Net.Movies.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Net.Movies.Application.AppServices
{
    public class CommonUserAppService : AppService, ICommonUserAppService
    {
        protected readonly IMapper _mapper;
        protected readonly ICommonUserService _service;

        public CommonUserAppService(IMapper mapper, ICommonUserService service)
        {
            _mapper = mapper;
            _service = service;
        }

        public AppServiceResponse Select(PaginationInputViewModel filter)
        {
            var response = _service.Select();

            if (response == null)
                return Fail(AppServiceMessages.REPONSE_IS_NULL_MESSAGE);

            if (response.Messages.Any())
                return Fail(response.Messages.ToArray());

            var serviceResult = _mapper.Map<IEnumerable<CommonUserOutputViewModel>>(response.Result);

            QueryPagination<CommonUserOutputViewModel> query = 
                new QueryPagination<CommonUserOutputViewModel>(serviceResult.AsQueryable(), filter.Take, filter.Page, filter.Ascending, x => x.Name);

            var result = query.Filter().ToList();

            return Ok(new CommonUserOutputPaginationViewModel
            {
                Items = result,
                TotalItems = serviceResult.Count()
            });
        }

        public AppServiceResponse Insert(CommonUserInputViewModel input)
        {
            var model = _mapper.Map<CommonUser>(input);

            var response = _service.Add(model);

            if (response == null)
                return Fail(AppServiceMessages.REPONSE_IS_NULL_MESSAGE);

            if (response.Messages.Any())
                return Fail(response.Messages.ToArray());

            return Ok(response.Result);
        }

        public AppServiceResponse Update(Guid id, CommonUserInputViewModel input)
        {
            var model = _mapper.Map<CommonUser>(input);

            var response = _service.Update(id, model);

            if (response == null)
                return Fail(AppServiceMessages.REPONSE_IS_NULL_MESSAGE);

            if (response.Messages.Any())
                return Fail(response.Messages.ToArray());

            var result = _mapper.Map<CommonUserOutputViewModel>(response.Result);

            return Ok(result);
        }


        public AppServiceResponse Delete(Guid id)
        {
            var response = _service.Delete(id);

            if (response == null)
                return Fail(AppServiceMessages.REPONSE_IS_NULL_MESSAGE);

            if (response.Messages.Any())
                return Fail(response.Messages.ToArray());

            return Ok();
        }

    }
}
