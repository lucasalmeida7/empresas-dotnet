﻿
using AutoMapper;
using Net.Movies.Application.Comunication;
using Net.Movies.Application.Contracts;
using Net.Movies.Application.ViewModels.AdminUser;
using Net.Movies.Domain.Contracts;
using Net.Movies.Domain.Entities;
using System;
using System.Linq;

namespace Net.Movies.Application.AppServices
{
    public class AdminUserAppService : AppService, IAdminUserAppService
    {
        protected readonly IMapper _mapper;
        protected readonly IAdminUserService _service;

        public AdminUserAppService(IMapper mapper, IAdminUserService service)
        {
            _mapper = mapper;
            _service = service;
        }

        public AppServiceResponse Insert(AdminUserInputViewModel input)
        {
            var model = _mapper.Map<AdminUser>(input);

            var response = _service.Add(model);

            if (response == null)
                return Fail(AppServiceMessages.REPONSE_IS_NULL_MESSAGE);

            if (response.Messages.Any())
                return Fail(response.Messages.ToArray());

            return Ok(response.Result);
        }

        public AppServiceResponse Update(Guid id, AdminUserInputViewModel input)
        {
            var model = _mapper.Map<AdminUser>(input);

            var response = _service.Update(id, model);

            if (response == null)
                return Fail(AppServiceMessages.REPONSE_IS_NULL_MESSAGE);

            if (response.Messages.Any())
                return Fail(response.Messages.ToArray());

            var result = _mapper.Map<AdminUserOutputViewModel>(response.Result);

            return Ok(result);
        }


        public AppServiceResponse Delete(Guid id)
        {
            var response = _service.Delete(id);

            if (response == null)
                return Fail(AppServiceMessages.REPONSE_IS_NULL_MESSAGE);

            if (response.Messages.Any())
                return Fail(response.Messages.ToArray());

            return Ok();
        }

    }
}
