﻿using AutoMapper;
using Net.Movies.Application.Comunication;
using Net.Movies.Application.Contracts;
using Net.Movies.Application.ViewModels.Actor;
using Net.Movies.Domain.Contracts;
using Net.Movies.Domain.Entities;
using System.Linq;

namespace Net.Movies.Application.AppServices
{
    public class ActorAppService : AppService, IActorAppService
    {
        private readonly IMapper _mapper;
        private readonly IActorService _service;

        public ActorAppService(IMapper mapper, IActorService service)
        {
            _mapper = mapper;
            _service = service;
        }

        public AppServiceResponse Insert(ActorInputViewModel input)
        {
            var model = _mapper.Map<Actor>(input);

            var response = _service.Add(model);

            if (response.Messages.Any())
                return Fail(response.Messages.ToArray());

            return Ok(response.Result);
        }
    }
}
