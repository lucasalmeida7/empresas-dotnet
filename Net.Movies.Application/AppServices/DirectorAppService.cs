﻿using AutoMapper;
using Net.Movies.Application.Comunication;
using Net.Movies.Application.Contracts;
using Net.Movies.Application.ViewModels.Director;
using Net.Movies.Domain.Contracts;
using Net.Movies.Domain.Entities;
using System.Linq;

namespace Net.Movies.Application.AppServices
{
    public class DirectorAppService : AppService, IDirectorAppService
    {
        private readonly IMapper _mapper;
        private readonly IDirectorService _service;

        public DirectorAppService(IMapper mapper, IDirectorService service)
        {
            _mapper = mapper;
            _service = service;
        }

        public AppServiceResponse Insert(DirectorInputViewModel input)
        {
            var model = _mapper.Map<Director>(input);

            var response = _service.Add(model);

            if (response.Messages.Any())
                return Fail(response.Messages.ToArray());

            return Ok(response.Result);
        }
    }
}
