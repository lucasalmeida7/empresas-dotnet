﻿using AutoMapper;
using Net.Movies.Application.Comunication;
using Net.Movies.Application.Contracts;
using Net.Movies.Application.Pagination;
using Net.Movies.Application.ViewModels.Actor;
using Net.Movies.Application.ViewModels.Gender;
using Net.Movies.Application.ViewModels.Movie;
using Net.Movies.Application.ViewModels.Pagination;
using Net.Movies.Domain.Contracts;
using Net.Movies.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Net.Movies.Application.AppServices
{
    public class MovieAppService : AppService, IMovieAppService
    {
        private readonly IMapper _mapper;
        private readonly IMovieService _service;
        private readonly IMovieActorService _movieActorservice;
        private readonly IMovieGenderService _movieGenderservice;

        public MovieAppService(IMapper mapper, IMovieService service, IMovieActorService movieActorservice, IMovieGenderService movieGenderservice)
        {
            _mapper = mapper;
            _service = service;
            _movieActorservice = movieActorservice;
            _movieGenderservice = movieGenderservice;
        }

        public AppServiceResponse Insert(MovieInputViewModel input)
        {
            var model = _mapper.Map<Movie>(input);

            var response = _service.Add(model);

            if (response == null)
                return Fail(AppServiceMessages.REPONSE_IS_NULL_MESSAGE);

            if (response.Messages.Any())
                return Fail(response.Messages.ToArray());

            foreach (var actorId in input.Actors)
            {
                var actorReponse = _movieActorservice.Add(new MovieActor { MovieId = response.Result, ActorId = actorId });
                if (actorReponse == null)
                    return Fail(AppServiceMessages.REPONSE_IS_NULL_MESSAGE);

                if (actorReponse.Messages.Any())
                    return Fail(actorReponse.Messages.ToArray());
            }

            foreach (var genderId in input.Genders)
            {
                var genderResponse = _movieGenderservice.Add(new MovieGender { MovieId = response.Result, GenderId = genderId });
                if (genderResponse == null)
                    return Fail(AppServiceMessages.REPONSE_IS_NULL_MESSAGE);

                if (genderResponse.Messages.Any())
                    return Fail(genderResponse.Messages.ToArray());
            }

            return Ok(response.Result);
        }

        public AppServiceResponse Select(Guid id)
        {
            var response = _service.SelectFull(id);

            if (response == null)
                return Fail(AppServiceMessages.REPONSE_IS_NULL_MESSAGE);

            if (response.Messages.Any())
                return Fail(response.Messages.ToArray());

            var result = _mapper.Map<MovieInfoOutputViewModel>(response.Result);

            var actors = response.Result.MovieActors.Select(x => x.Actor);
            var genders = response.Result.MovieGenders.Select(x => x.Gender);

            result.Actors = _mapper.Map<IEnumerable<ActorOutputViewModel>>(actors);
            result.Genders = _mapper.Map<IEnumerable<GenderOutputViewModel>>(genders);

            return Ok(result);
        }

        public AppServiceResponse Select(PaginationInputViewModel filter)
        {
            var response = _service.Select();

            if (response == null)
                return Fail(AppServiceMessages.REPONSE_IS_NULL_MESSAGE);

            if (response.Messages.Any())
                return Fail(response.Messages.ToArray());

            var serviceResult = _mapper.Map<IEnumerable<MovieOutputViewModel>>(response.Result);

            QueryPagination<MovieOutputViewModel> query =
                new QueryPagination<MovieOutputViewModel>(serviceResult.AsQueryable(), filter.Take, filter.Page, filter.Ascending, x => x.Title);

            var result = query.Filter().ToList();

            return Ok(new MovieOutputPaginationViewModel
            {
                Items = result,
                TotalItems = serviceResult.Count()
            });
        }
    }
}
