﻿
namespace Net.Movies.Application.ViewModels.Pagination
{
    /// <summary>
    /// Dados de retorno de paginação
    /// </summary>
    public class PaginationOutputViewModel
    {
        /// <summary>
        /// Total de itens
        /// </summary>
        public int TotalItems { get; set; }
    }
}
