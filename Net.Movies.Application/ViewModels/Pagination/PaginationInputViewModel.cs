﻿using Net.Movies.Application.Comunication;
using System.ComponentModel.DataAnnotations;

namespace Net.Movies.Application.ViewModels.Pagination
{
    /// <summary>
    /// Dados de filtro para paginação em pesquisa
    /// </summary>
    public class PaginationInputViewModel
    {
        /// <summary>
        ///  Página atual
        /// </summary>
        [Required(ErrorMessage = AppServiceMessages.ViewModel.REQUIRED_MESSAGE)]
        public int Page { get; set; }

        /// <summary>
        /// Quantidade de itens por página
        /// </summary>
        [Required(ErrorMessage = AppServiceMessages.ViewModel.REQUIRED_MESSAGE)]
        public int Take { get; set; }

        /// <summary>
        /// Ordenação da busca
        /// </summary>
        [Required(ErrorMessage = AppServiceMessages.ViewModel.REQUIRED_MESSAGE)]
        public bool Ascending { get; set; }
    }
}
