﻿
using Net.Movies.Application.Comunication;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Net.Movies.Application.ViewModels.MovieRating
{
    /// <summary>
    /// Dados necessário para o cadastro/atualização de classificação de filme
    /// </summary>
    public class MovieRatingInputViewModel
    {
        /// <summary>
        /// Id do filme
        /// </summary>
        [DisplayName(nameof(MovieId))]
        [Required(ErrorMessage = AppServiceMessages.ViewModel.REQUIRED_MESSAGE)]
        public Guid MovieId { get; set; }

        /// <summary>
        /// Id do usuário
        /// </summary>
        [DisplayName(nameof(CommonUserId))]
        [Required(ErrorMessage = AppServiceMessages.ViewModel.REQUIRED_MESSAGE)]
        public Guid CommonUserId { get; set; }

        /// <summary>
        /// Nota do filme
        /// </summary>
        [DisplayName(nameof(Score))]
        [Required(ErrorMessage = AppServiceMessages.ViewModel.REQUIRED_MESSAGE)]
        public int Score { get; set; }
    }
}
