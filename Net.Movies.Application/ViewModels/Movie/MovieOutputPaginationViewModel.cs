﻿
using Net.Movies.Application.ViewModels.Pagination;
using System;
using System.Collections.Generic;

namespace Net.Movies.Application.ViewModels.Movie
{
    /// <summary>
    /// Lista de dados de filme paginados
    /// </summary>
    public class MovieOutputPaginationViewModel : PaginationOutputViewModel
    {
        public MovieOutputPaginationViewModel()
        {
            Items = new List<MovieOutputViewModel>();
        }

        /// <summary>
        /// Lista de itens
        /// </summary>
        public IEnumerable<MovieOutputViewModel> Items { get; set; }
    }

    /// <summary>
    /// Dados de filme
    /// </summary>
    public class MovieOutputViewModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid Id { get; set; }
        
        /// <summary>
        /// Título
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Ano
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Nota Média
        /// </summary>
        public decimal MeanScore { get; set; }

        /// <summary>
        /// Nome do Diretor
        /// </summary>
        public string Director { get; set; }
    }
}
