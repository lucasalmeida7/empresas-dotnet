﻿
using Net.Movies.Application.ViewModels.Actor;
using Net.Movies.Application.ViewModels.Director;
using Net.Movies.Application.ViewModels.Gender;
using System;
using System.Collections.Generic;

namespace Net.Movies.Application.ViewModels.Movie
{
    /// <summary>
    /// Dados de informaçoes de filme
    /// </summary>
    public class MovieInfoOutputViewModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid Id { get; set; }
        
        /// <summary>
        /// Título
        /// </summary>
        public string Title { get; set; }
        
        /// <summary>
        /// Ano
        /// </summary>
        public int Year { get; set; }
        
        /// <summary>
        /// Nota Média
        /// </summary>
        public decimal MeanScore { get; set; }
        
        /// <summary>
        /// Diretor
        /// </summary>
        public DirectorOutputViewModel Director { get; set; }
        
        /// <summary>
        /// Atores/Atrizes
        /// </summary>
        public IEnumerable<ActorOutputViewModel> Actors { get; set; }
        
        /// <summary>
        /// Gêneros
        /// </summary>
        public IEnumerable<GenderOutputViewModel> Genders { get; set; }
    }
}
