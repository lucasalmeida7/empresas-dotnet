﻿
using Net.Movies.Application.Comunication;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Net.Movies.Application.ViewModels.Movie
{
    /// <summary>
    /// Dados para cadastro de filme 
    /// </summary>
    public class MovieInputViewModel
    {
        /// <summary>
        /// Título
        /// </summary>
        [DisplayName(nameof(Title))]
        [Required(ErrorMessage = AppServiceMessages.ViewModel.REQUIRED_MESSAGE)]
        [MaxLength(100)]
        public string Title { get; set; }

        /// <summary>
        /// Ano
        /// </summary>
        [DisplayName(nameof(Year))]
        [Required(ErrorMessage = AppServiceMessages.ViewModel.REQUIRED_MESSAGE)]
        public int Year { get; set; }

        /// <summary>
        /// Id do diretor
        /// </summary>
        [DisplayName(nameof(DirectorId))]
        [Required(ErrorMessage = AppServiceMessages.ViewModel.REQUIRED_MESSAGE)]
        public Guid DirectorId { get; set; }

        /// <summary>
        /// Id do genêros do filme
        /// </summary>
        [DisplayName(nameof(Genders))]
        [Required(ErrorMessage = AppServiceMessages.ViewModel.REQUIRED_MESSAGE)]
        public IEnumerable<Guid> Genders { get; set; }

        /// <summary>
        /// Id do atores/atrizes do filme
        /// </summary>
        [DisplayName(nameof(Actors))]
        [Required(ErrorMessage = AppServiceMessages.ViewModel.REQUIRED_MESSAGE)]
        public IEnumerable<Guid> Actors { get; set; }
    }
}
