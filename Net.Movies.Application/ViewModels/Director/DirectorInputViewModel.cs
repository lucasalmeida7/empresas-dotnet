﻿
namespace Net.Movies.Application.ViewModels.Director
{
    /// <summary>
    /// Dados para cadastro/atualização de diretor
    /// </summary>
    public class DirectorInputViewModel
    {
        /// <summary>
        /// Nome
        /// </summary>
        public string Name { get; set; }
    }
}