﻿
using System;

namespace Net.Movies.Application.ViewModels.Director
{
    /// <summary>
    /// Dados de diretor(a)
    /// </summary>
    public class DirectorOutputViewModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Nome
        /// </summary>
        public string Name { get; set; }
    }
}
