﻿using System;

namespace Net.Movies.Application.ViewModels.Actor
{
    /// <summary>
    /// Dados de ator/atriz
    /// </summary>
    public class ActorOutputViewModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Nome
        /// </summary>
        public string Name { get; set; }
    }
}
