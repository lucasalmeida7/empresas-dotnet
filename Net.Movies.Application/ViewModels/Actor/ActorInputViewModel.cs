﻿
namespace Net.Movies.Application.ViewModels.Actor
{
    /// <summary>
    /// Dados para cadastro/atualização de ator/atriz
    /// </summary>
    public class ActorInputViewModel
    {
        /// <summary>
        /// Nome
        /// </summary>
        public string Name { get; set; }
    }
}
