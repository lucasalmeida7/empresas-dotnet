﻿using Net.Movies.Application.ViewModels.Pagination;
using System;
using System.Collections.Generic;

namespace Net.Movies.Application.ViewModels.CommonUser
{
    /// <summary>
    /// Lista de dados de usuário comum paginados
    /// </summary>
    public class CommonUserOutputPaginationViewModel : PaginationOutputViewModel
    {
        public CommonUserOutputPaginationViewModel()
        {
            Items = new List<CommonUserOutputViewModel>();
        }

        /// <summary>
        /// Lista de itens
        /// </summary>
        public IEnumerable<CommonUserOutputViewModel> Items { get; set; }
    }

    /// <summary>
    /// Dados de usuário comum
    /// </summary>
    public class CommonUserOutputViewModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Nome
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// E-mail
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Senha
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Data de criação
        /// </summary>
        public DateTime CreateDate { get; set; }
    }
}
