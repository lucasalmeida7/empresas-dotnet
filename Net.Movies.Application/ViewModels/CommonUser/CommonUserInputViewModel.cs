﻿
using Net.Movies.Application.Comunication;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Net.Movies.Application.ViewModels.CommonUser
{
    /// <summary>
    /// Dados para cadastro/atualização de usuário comum
    /// </summary>
    public class CommonUserInputViewModel
    {
        /// <summary>
        /// Nome
        /// </summary>
        [DisplayName(nameof(Name))]
        [Required(ErrorMessage = AppServiceMessages.ViewModel.REQUIRED_MESSAGE)]
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// E-mail
        /// </summary>
        [DisplayName(nameof(Email))]
        [Required(ErrorMessage = AppServiceMessages.ViewModel.REQUIRED_MESSAGE)]
        public string Email { get; set; }

        /// <summary>
        /// Senha
        /// </summary>
        [DisplayName(nameof(Password))]
        [Required(ErrorMessage = AppServiceMessages.ViewModel.REQUIRED_MESSAGE)]
        public string Password { get; set; }
    }
}
