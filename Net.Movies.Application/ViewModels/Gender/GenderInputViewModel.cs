﻿
using Net.Movies.Application.Comunication;
using System.ComponentModel.DataAnnotations;

namespace Net.Movies.Application.ViewModels.Gender
{
    /// <summary>
    /// Dados para cadastro/atualização de gênero
    /// </summary>
    public class GenderInputViewModel
    {
        /// <summary>
        /// Nome
        /// </summary>
        [Required(ErrorMessage = AppServiceMessages.ViewModel.REQUIRED_MESSAGE)]
        public string Name { get; set; }
    }
}
