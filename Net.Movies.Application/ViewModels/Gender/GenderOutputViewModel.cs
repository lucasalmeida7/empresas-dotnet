﻿
using System;

namespace Net.Movies.Application.ViewModels.Gender
{
    /// <summary>
    /// Dados de gênero de filme
    /// </summary>
    public class GenderOutputViewModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Nome
        /// </summary>
        public string Name { get; set; }
    }
}
