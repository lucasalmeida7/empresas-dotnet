﻿using System;

namespace Net.Movies.Application.ViewModels.AdminUser
{
    /// <summary>
    /// Dados de usuário administrador
    /// </summary>
    public class AdminUserOutputViewModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Nome
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// E-mail
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Senha
        /// </summary>
        public string Password { get; set; }
    }
}
