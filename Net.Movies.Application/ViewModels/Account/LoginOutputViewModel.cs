﻿
namespace Net.Movies.Application.ViewModels.Account
{
    /// <summary>
    /// Credencias de autenticação JWT
    /// </summary>
    public class LoginOutputViewModel
    {
        /// <summary>
        /// E-mail do usuário autenticado
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Senha do usuário autenticado
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Token (JWT) para autenticação
        /// </summary>
        public string Token { get; set; }
    }
}
