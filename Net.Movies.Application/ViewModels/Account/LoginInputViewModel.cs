﻿
using Net.Movies.Application.Comunication;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Net.Movies.Application.ViewModels.Account
{
    /// <summary>
    /// Dados necessários para a autenticação de usuário
    /// </summary>
    public class LoginInputViewModel
    {
        /// <summary>
        /// E-mail do usuário
        /// </summary>
        [DisplayName(nameof(Email))]
        [Required(ErrorMessage = AppServiceMessages.ViewModel.REQUIRED_MESSAGE)]
        public string Email { get; set; }

        /// <summary>
        /// Senha do usuário
        /// </summary>
        [DisplayName(nameof(Password))]
        [Required(ErrorMessage = AppServiceMessages.ViewModel.REQUIRED_MESSAGE)]
        public string Password { get; set; }
    }
}
