﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Net.Movies.Application.Contracts;
using Net.Movies.Application.ViewModels.MovieRating;

namespace Net.Movies.Api.Controllers
{
    /// <summary>
    ///  Operações de classificação de filme
    /// </summary>
    [ApiController]
    [Route("api/movie-rating")]
    public class MovieRatingController : ControllerBase
    {
        private readonly IMovieRatingAppService _service;

        public MovieRatingController(IMovieRatingAppService service)
        {
            _service = service;
        }

        /// <summary>Cadastra a nota de um filme definida por um usuário</summary>
        /// <param name="input">Dados necessários para o cadastro</param>
        /// <returns>Id da classificação do filme</returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(Guid), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [Authorize(Roles = "CommonUser")]
        [HttpPost]
        public IActionResult Insert(MovieRatingInputViewModel input)
        {
            var response = _service.Insert(input);

            if (response.StatusCode != 200)
                return new ObjectResult(response.Messages) { StatusCode = response.StatusCode };

            return Ok(response.Result);
        }
    }
}
