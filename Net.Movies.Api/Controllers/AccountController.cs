﻿using Microsoft.AspNetCore.Mvc;
using Net.Movies.Application.Contracts;
using Net.Movies.Application.ViewModels.Account;

namespace Net.Movies.Api.Controllers
{
    /// <summary>
    /// Operações de conta de usuário
    /// </summary>
    [ApiController]
    [Route("api/account")]
    public class AccountController : ControllerBase
    {
        private readonly IAccountAppService _service;

        public AccountController(IAccountAppService service)
        {
            _service = service;
        }

        /// <summary>Autentica o usuário</summary>
        /// <param name="input">Dados necessários para a autenticação</param>
        /// <returns>Credenciais de autenticação</returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(LoginOutputViewModel), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [HttpPost("login")]
        public IActionResult Login(LoginInputViewModel input)
        {
            var response = _service.Login(input);

            if (response.StatusCode != 200)
                return new ObjectResult(response.Messages) { StatusCode = response.StatusCode };

            return Ok(response.Result);
        }
    }
}
