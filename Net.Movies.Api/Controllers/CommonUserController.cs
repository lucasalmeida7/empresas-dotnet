﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Net.Movies.Application.Comunication;
using Net.Movies.Application.Contracts;
using Net.Movies.Application.ViewModels.CommonUser;
using Net.Movies.Application.ViewModels.Pagination;
using System;
using System.ComponentModel.DataAnnotations;

namespace Net.Movies.Api.Controllers
{
    /// <summary>
    ///  Operações de usuário comum
    /// </summary>
    [ApiController]
    [Route("api/common-user")]
    public class CommonUserController : ControllerBase
    {
        private readonly ICommonUserAppService _service;

        public CommonUserController(ICommonUserAppService service)
        {
            _service = service;
        }

        /// <summary>Cadastra um usuário</summary>
        /// <param name="input">Dados necessários para o cadastro</param>
        /// <returns>Id do usuário cadastrado</returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(Guid), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [Authorize]
        [HttpPost]
        public IActionResult Insert(CommonUserInputViewModel input)
        {
            var response = _service.Insert(input);

            if (response.StatusCode != 200)
                return new ObjectResult(response.Messages) { StatusCode = response.StatusCode };

            return Ok(response.Result);
        }

        /// <summary>Atualiza o cadastro de um usuário</summary>
        /// <param name="id">Id do usuário</param>
        /// <param name="input">Dados necessários para a atualização</param>
        /// <returns>Usuário atualizado</returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [Authorize]
        [HttpPut("{id}")]
        public IActionResult Update([Required(ErrorMessage = AppServiceMessages.ViewModel.REQUIRED_MESSAGE)] Guid id, CommonUserInputViewModel input)
        {
            var response = _service.Update(id, input);

            if (response.StatusCode != 200)
                return new ObjectResult(response.Messages) { StatusCode = response.StatusCode };

            return Ok(response.Result);
        }

        /// <summary>Remove um usuário</summary>
        /// <param name="id">Id do usuário</param>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [Authorize]
        [HttpDelete("{id}")]
        public IActionResult Delete([Required(ErrorMessage = AppServiceMessages.ViewModel.REQUIRED_MESSAGE)] Guid id)
        {
            var response = _service.Delete(id);

            if (response.StatusCode != 200)
                return new ObjectResult(response.Messages) { StatusCode = response.StatusCode };

            return Ok(response.Result);
        }

        /// <summary>Busca os usuários ativos no sistema</summary>
        /// <returns>Lista de usuários ativos</returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [Authorize]
        [HttpGet]
        public IActionResult Select([FromQuery] PaginationInputViewModel filter)
        {
            var response = _service.Select(filter);

            if (response.StatusCode != 200)
                return new ObjectResult(response.Messages) { StatusCode = response.StatusCode };

            return Ok(response.Result);
        }
    }
}
