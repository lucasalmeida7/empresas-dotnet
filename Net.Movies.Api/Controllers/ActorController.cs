﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Net.Movies.Application.Contracts;
using Net.Movies.Application.ViewModels.Actor;

namespace Net.Movies.Api.Controllers
{
    /// <summary>
    ///  Operações de ator/atriz
    /// </summary>
    [ApiController]
    [Route("api/actor")]
    public class ActorController : ControllerBase
    {
        protected readonly IActorAppService _service;

        public ActorController(IActorAppService service)
        {
            _service = service;
        }

        /// <summary>Cadastra de ator/atriz</summary>
        /// <param name="input">Dados necessários para o cadastro</param>
        /// <returns>Id do ator/atriz cadastrado</returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(Guid), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [Authorize(Roles = "AdminUser")]
        [HttpPost]
        public IActionResult Insert(ActorInputViewModel input)
        {
            var response = _service.Insert(input);

            if (response.StatusCode != 200)
                return new ObjectResult(response.Messages) { StatusCode = response.StatusCode };

            return Ok(response.Result);
        }
    }
}
