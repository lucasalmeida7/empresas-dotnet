﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Net.Movies.Application.Comunication;
using Net.Movies.Application.Contracts;
using Net.Movies.Application.ViewModels.AdminUser;
using System;
using System.ComponentModel.DataAnnotations;

namespace Net.Movies.Api.Controllers
{
    /// <summary>
    /// Operações de usuário administrador
    /// </summary>
    [ApiController]
    [Route("api/admin-user")]
    public class AdminUserController : ControllerBase
    {
        private readonly IAdminUserAppService _service;

        public AdminUserController(IAdminUserAppService service)
        {
            _service = service;
        }

        /// <summary>Cadastra um usuário administrador</summary>
        /// <param name="input">Dados necessários para o cadastro</param>
        /// <returns>Id do usuário cadastrado</returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(Guid), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [Authorize(Roles = "AdminUser")]
        [HttpPost]
        public IActionResult Insert(AdminUserInputViewModel input)
        {
            var response = _service.Insert(input);

            if (response.StatusCode != 200)
                return new ObjectResult(response.Messages) { StatusCode = response.StatusCode };

            return Ok(response.Result);
        }

        /// <summary>Atualiza o cadastro de um usuário administrador</summary>
        /// <param name="id">Id do usuário</param>
        /// <param name="input">Dados necessários para a atualização</param>
        /// <returns>Usuário atualizado</returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [Authorize(Roles = "AdminUser")]
        [HttpPut("{id}")]
        public IActionResult Update([Required(ErrorMessage = AppServiceMessages.ViewModel.REQUIRED_MESSAGE)] Guid id, AdminUserInputViewModel input)
        {
            var response = _service.Update(id, input);

            if (response.StatusCode != 200)
                return new ObjectResult(response.Messages) { StatusCode = response.StatusCode };

            return Ok(response.Result);
        }

        /// <summary>Remove um usuário administrador</summary>
        /// <param name="id">Id do usuário</param>
        /// <returns></returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [Authorize(Roles = "AdminUser")]
        [HttpDelete("{id}")]
        public IActionResult Delete([Required(ErrorMessage = AppServiceMessages.ViewModel.REQUIRED_MESSAGE)] Guid id)
        {
            var response = _service.Delete(id);

            if (response.StatusCode != 200)
                return new ObjectResult(response.Messages) { StatusCode = response.StatusCode };

            return Ok(response.Result);
        }
    }
}
