﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Net.Movies.Application.Contracts;
using Net.Movies.Application.ViewModels.Gender;
using System;

namespace Net.Movies.Api.Controllers
{
    /// <summary>
    ///  Operações de gênero
    /// </summary>
    [ApiController]
    [Route("api/gender")]
    public class GenderController : ControllerBase
    {
        protected readonly IGenderAppService _service;

        public GenderController(IGenderAppService service)
        {
            _service = service;
        }

        /// <summary>Cadastra de gênero</summary>
        /// <param name="input">Dados necessários para o cadastro</param>
        /// <returns>Id do gênero cadastrado</returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(Guid), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [Authorize(Roles = "AdminUser")]
        [HttpPost]
        public IActionResult Insert(GenderInputViewModel input)
        {
            var response = _service.Insert(input);

            if (response.StatusCode != 200)
                return new ObjectResult(response.Messages) { StatusCode = response.StatusCode };

            return Ok(response.Result);
        }
    }
}
