﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Net.Movies.Application.Contracts;
using Net.Movies.Application.ViewModels.Director;

namespace Net.Movies.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DirectorController : ControllerBase
    {
        protected readonly IDirectorAppService _service;

        public DirectorController(IDirectorAppService service)
        {
            _service = service;
        }

        /// <summary>Cadastra de diretor</summary>
        /// <param name="input">Dados necessários para o cadastro</param>
        /// <returns>Id do diretor cadastrado</returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(Guid), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [Authorize(Roles = "AdminUser")]
        [HttpPost]
        public IActionResult Insert(DirectorInputViewModel input)
        {
            var response = _service.Insert(input);

            if (response.StatusCode != 200)
                return new ObjectResult(response.Messages) { StatusCode = response.StatusCode };

            return Ok(response.Result);
        }
    }
}
