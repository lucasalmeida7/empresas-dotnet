﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Net.Movies.Application.Comunication;
using Net.Movies.Application.Contracts;
using Net.Movies.Application.ViewModels;
using Net.Movies.Application.ViewModels.Movie;
using Net.Movies.Application.ViewModels.Pagination;
using System;
using System.ComponentModel.DataAnnotations;

namespace Net.Movies.Api.Controllers
{
    /// <summary>
    ///  Operações de filme
    /// </summary>
    [ApiController]
    [Route("api/movie")]
    public class MovieController : ControllerBase
    {
        protected readonly IMovieAppService _service;

        public MovieController(IMovieAppService service)
        {
            _service = service;
        }

        /// <summary>Cadastra um filme</summary>
        /// <param name="input">Dados necessários para o cadastro</param>
        /// <returns>Id do filme cadastrado</returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(Guid), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [Authorize(Roles = "AdminUser")]
        [HttpPost]
        public IActionResult Insert(MovieInputViewModel input)
        {
            var response = _service.Insert(input);

            if (response.StatusCode != 200)
                return new ObjectResult(response.Messages) { StatusCode = response.StatusCode };

            return Ok(response.Result);
        }

        /// <summary>Busca um filme por id</summary>
        /// <param name="id">Id do filme</param>
        /// <returns>Filme encontrado</returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [Authorize]
        [HttpGet("{id}")]
        public IActionResult Select([Required(ErrorMessage = AppServiceMessages.ViewModel.REQUIRED_MESSAGE)] Guid id)
        {
            var response = _service.Select(id);

            if (response.StatusCode != 200)
                return new ObjectResult(response.Messages) { StatusCode = response.StatusCode };

            return Ok(response.Result);
        }

        /// <summary>Busca os filmes cadastrados</summary>
        /// <returns>Lista de filmes cadastrados</returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [Authorize]
        [HttpGet]
        public IActionResult Select([FromQuery] PaginationInputViewModel filter)
        {
            var response = _service.Select(filter);

            if (response.StatusCode != 200)
                return new ObjectResult(response.Messages) { StatusCode = response.StatusCode };

            return Ok(response.Result);
        }
    }
}
