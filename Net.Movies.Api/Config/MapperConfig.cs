﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Net.Movies.Application.Automapper;

namespace Net.Movies.Api.Config
{
    public static class MapperConfig
    {
        public static void AddMapper(this IServiceCollection services)
        {
            var config = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = config.CreateMapper();

            services.AddSingleton(mapper);
        }
    }
}
