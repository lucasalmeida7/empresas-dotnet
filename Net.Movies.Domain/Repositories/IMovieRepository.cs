﻿using Net.Movies.Domain.Entities;

namespace Net.Movies.Domain.Repositories
{
    public interface IMovieRepository : IRepository<Movie>
    {
    }
}
