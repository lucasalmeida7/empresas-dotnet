﻿using Net.Movies.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Net.Movies.Domain.Repositories
{
    public interface IRepository<T> where T : Entity
    {
        IQueryable<T> Select();
        IQueryable<T> Select(params Expression<Func<T, object>>[] includes);
        IQueryable<T> Select(Func<T, bool> predicated);
        IQueryable<T> Select(Func<T, bool> predicated, params Expression<Func<T, object>>[] includes);
        
        T Select(Guid id);
        T Select(Guid id, params Expression<Func<T, object>>[] includes);

        Guid Insert(T entity);
        void Update(T entity);
        void Delete(Guid id);
        void Delete(IEnumerable<T> entities);
    }
}
