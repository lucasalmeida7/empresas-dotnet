﻿using System;
using System.Collections.Generic;

namespace Net.Movies.Domain.Entities
{
    public class Actor : Entity
    {
        public string Name { get; private set; }

        public ICollection<MovieActor> MovieActors { get; set; }

        public override bool Validate()
        {
            if (string.IsNullOrWhiteSpace(Name))
                AddNotification("O nome deve ser informado");

            if (Name.Length > 50)
                AddNotification("O nome não deve possuir tamanho maior que 50");

            return Valid;
        }
    }
}
