﻿using System;
using System.Collections.Generic;

namespace Net.Movies.Domain.Entities
{
    public class CommonUser : User
    {
        public CommonUser(string name, string email, string password) : base(name, email, password)
        {
            CreateDate = DateTime.Now;
        }

        public DateTime CreateDate { get; set; }

        public ICollection<MovieRating> MovieRatings { get; set; }

        public override bool Validate()
        {
            return base.Validate();
        }
    }
}
