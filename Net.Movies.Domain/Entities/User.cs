﻿using Net.Movies.Domain.ValueObjects;
using System;
using System.Text.RegularExpressions;

namespace Net.Movies.Domain.Entities
{
    public abstract class User : Entity
    {
        public User(string name, string email, string password)
        {
            Name = name;
            Email = email;
            Password = password;
            Active = true;

            if (string.IsNullOrWhiteSpace(name))
                AddNotification("O nome deve ser informado");

            if (name.Length > 50)
                AddNotification("O nome não deve possuir tamanho maior que 50");

            if (string.IsNullOrWhiteSpace(email))
                AddNotification("O endereço deve ser informado");

            if (email.Length > 50)
                AddNotification("O endereço não deve possuir tamanho maior que 50");

            if (!Regex.IsMatch(email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"))
                AddNotification("Formato de e-mail inválido");

            if (string.IsNullOrWhiteSpace(password))
                AddNotification("A senha deve ser informada");

            if (password.Length > 20)
                AddNotification("A senha não deve possuir tamanho maior que 20");
        }

        public string Name { get; private set; }

        public string Email { get; private set; }

        public string Password { get; private set; }

        public bool Active { get; private set; }

        public void Deactivate()
        {
            Active = false;
        }

        public override bool Validate()
        {
            if (string.IsNullOrWhiteSpace(Name))
                AddNotification("O nome deve ser informado");

            if (Name.Length > 50)
                AddNotification("O nome não deve possuir tamanho maior que 50");

            if (string.IsNullOrWhiteSpace(Email))
                AddNotification("O endereço deve ser informado");

            if (Email.Length > 50)
                AddNotification("O endereço não deve possuir tamanho maior que 50");

            if (!Regex.IsMatch(Email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"))
                AddNotification("Formato de e-mail inválido");

            if (string.IsNullOrWhiteSpace(Password))
                AddNotification("A senha deve ser informada");

            if (Password.Length > 20)
                AddNotification("A senha não deve possuir tamanho maior que 20");

            return Valid;
        }
    }
}