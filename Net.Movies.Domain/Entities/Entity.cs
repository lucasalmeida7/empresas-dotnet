﻿using Net.Movies.Domain.Comunication;
using Net.Movies.Domain.Notification;
using System;

namespace Net.Movies.Domain.Entities
{
    public abstract class Entity : Notifiable
    {
        protected Entity()
        {
        }

        private Guid id;

        public virtual Guid Id
        {
            get { return id; }
            set {
                if (Guid.Empty == value)
                    AddNotification(ServiceMessages.INVALID_ID_MESSAGE);
                else
                    Id = value;
            }
        }

        public abstract bool Validate();
    }
}
