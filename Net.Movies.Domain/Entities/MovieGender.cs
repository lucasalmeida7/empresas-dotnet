﻿using System;

namespace Net.Movies.Domain.Entities
{
    public class MovieGender : Entity
    {
        public MovieGender()
        {
        }

        public Movie Movie { get; set; }
        public Guid MovieId { get; set; }
        public Gender Gender { get; set; }
        public Guid GenderId { get; set; }

        public override bool Validate()
        {
            return Valid;
        }
    }
}
