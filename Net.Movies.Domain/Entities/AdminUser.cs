﻿using System;

namespace Net.Movies.Domain.Entities
{
    public class AdminUser : User
    {
        public AdminUser(string name, string email, string password) : base(name, email, password)
        {
        }

        public override bool Validate()
        {
            return base.Validate();
        }
    }
}
