﻿using System;

namespace Net.Movies.Domain.Entities
{
    public class MovieRating : Entity
    {
        public MovieRating(Guid movieId, Guid commonUserId, int score)
        {
            MovieId = movieId;
            CommonUserId = commonUserId;
            Score = score;
        }

        public Movie Movie { get; set; }
        public Guid MovieId { get; set; }
        public CommonUser CommonUser { get; set; }
        public Guid CommonUserId { get; set; }
        public int Score { get; private set; }

        public override bool Validate()
        {
            if (Score < 0 || Score > 4)
                AddNotification("A nota deve ser entre 0 e 4");

            return Valid;
        }
    }
}
