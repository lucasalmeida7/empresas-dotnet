﻿using System;
using System.Collections.Generic;

namespace Net.Movies.Domain.Entities
{
    public class Movie : Entity
    {
        public Movie(string title, int year, Guid directorId)
        {
            Title = title;
            Year = year;
            DirectorId = directorId;

            if (string.IsNullOrWhiteSpace(title))
                AddNotification("O título deve ser informado");

            if (title.Length > 100)
                AddNotification("O título não deve possuir tamanho maior que 100");

            if (year.ToString().Length != 4)
                AddNotification("O ano deve possuir 4 digitos");
        }

        public string Title { get; private set; }
        public int Year { get; private set; }
        public Guid DirectorId { get; private set; }

        public Director Director { get; private set; }
        public ICollection<MovieGender> MovieGenders { get; set; }
        public ICollection<MovieActor> MovieActors { get; set; }
        public ICollection<MovieRating> MovieRatings { get; set; }

        public override bool Validate()
        {
            if (string.IsNullOrWhiteSpace(Title))
                AddNotification("O título deve ser informado");

            if (Title.Length > 100)
                AddNotification("O título não deve possuir tamanho maior que 100");

            if (Year.ToString().Length != 4)
                AddNotification("O ano deve possuir 4 digitos");

            return Valid;
        }
    }
}
