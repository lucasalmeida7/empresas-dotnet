﻿using System;

namespace Net.Movies.Domain.Entities
{
    public class MovieActor : Entity
    {
        public MovieActor()
        {
        }

        public Movie Movie { get; set; }
        public Guid MovieId { get; set; }
        public Actor Actor { get; set; }
        public Guid ActorId { get; set; }

        public override bool Validate()
        {
            return Valid;
        }
    }
}
