﻿
using System.Collections.Generic;
using System.Linq;

namespace Net.Movies.Domain.Comunication
{
    public class ServiceResponse<T>
    {
        public ServiceResponse()
        {
            Messages = new List<string>();
        }

        public T Result { get; set; }

        public IList<string> Messages { get; set; }

        public void AddMessages(IList<string> messages)
        {
            messages.ToList().ForEach(x => Messages.Add(x));
        }

        public void AddMessages(IReadOnlyCollection<string> messages)
        {
            messages.ToList().ForEach(x => Messages.Add(x));
        }
    }
}
