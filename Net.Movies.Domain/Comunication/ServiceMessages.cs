﻿
namespace Net.Movies.Domain.Comunication
{
    public static class ServiceMessages
    {
        public const string INVALID_ID_MESSAGE = "O Id informado é inválido";

        public const string ID_NOT_EXISTS_MESSAGE = "O Id informado não existe";
    }
}
