﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Net.Movies.Domain.Notification
{
    public abstract class Notifiable
    {
        private List<string> _notifications;

        public Notifiable()
        {
            _notifications = new List<string>();
        }

        public IReadOnlyCollection<string> Notifications { get { return _notifications.ToArray(); } }

        public bool Valid => !_notifications.Any();

        public void AddNotification(string message)
        {
            _notifications.Add(message);
        }

        public void AddNotifications(params Notifiable[] items)
        {
            foreach (var item in items)
            {
                _notifications.AddRange(item.Notifications);
            }
        }
    }
}
