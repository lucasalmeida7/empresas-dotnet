﻿using Net.Movies.Domain.Comunication;
using Net.Movies.Domain.Contracts;
using Net.Movies.Domain.Entities;
using Net.Movies.Domain.Repositories;
using System;
using System.Linq;

namespace Net.Movies.Domain.Services
{
    public class GenderService : IGenderService
    {
        protected readonly IGenderRepository _repository;

        public GenderService(IGenderRepository repository)
        {
            _repository = repository;
        }

        public ServiceResponse<Guid> Add(Gender gender)
        {
            var response = new ServiceResponse<Guid>();

            if (!gender.Validate())
                response.AddMessages(gender.Notifications);

            if (!response.Messages.Any())
            {
                response.Result = _repository.Insert(gender);
            }

            return response;
        }
    }
}
