﻿
using Net.Movies.Domain.Comunication;
using Net.Movies.Domain.Contracts;
using Net.Movies.Domain.Entities;
using Net.Movies.Domain.Repositories;
using System;
using System.Linq;

namespace Net.Movies.Domain.Services
{
    public class MovieGenderService : IMovieGenderService
    {
        private readonly IMovieGenderRepository _repository;

        public MovieGenderService(IMovieGenderRepository repository)
        {
            _repository = repository;
        }

        public ServiceResponse<Guid> Add(MovieGender movieGender)
        {
            var response = new ServiceResponse<Guid>();

            if (movieGender.Validate())
                response.AddMessages(movieGender.Notifications);

            if (!response.Messages.Any())
            {
                response.Result = _repository.Insert(movieGender);
            }

            return response;
        }
    }
}
