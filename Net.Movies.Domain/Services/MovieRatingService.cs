﻿using Net.Movies.Domain.Comunication;
using Net.Movies.Domain.Contracts;
using Net.Movies.Domain.Entities;
using Net.Movies.Domain.Repositories;
using System;
using System.Linq;

namespace Net.Movies.Domain.Services
{
    public class MovieRatingService : IMovieRatingService
    {
        private readonly IMovieRatingRepository _repository;

        public MovieRatingService(IMovieRatingRepository repository)
        {
            _repository = repository;
        }

        public ServiceResponse<Guid> Add(MovieRating movieRating)
        {
            var response = new ServiceResponse<Guid>();

            if (movieRating.Validate())
                response.AddMessages(movieRating.Notifications);

            var entity = _repository.Select(x => x.MovieId == movieRating.MovieId && x.CommonUserId == movieRating.CommonUserId).FirstOrDefault();

            if (entity != null)
                response.Messages.Add("Este usuário já possui uma avaliação para este filme");

            if (!response.Messages.Any())
            {
                response.Result = _repository.Insert(movieRating);
            }

            return response;
        }
    }
}
