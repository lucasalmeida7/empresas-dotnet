﻿using Net.Movies.Domain.Comunication;
using Net.Movies.Domain.Contracts;
using Net.Movies.Domain.Entities;
using Net.Movies.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Net.Movies.Domain.Services
{
    public class CommonUserService : ICommonUserService
    {
        private readonly ICommonUserRepository _repository;

        public CommonUserService(ICommonUserRepository repository)
        {
            _repository = repository;
        }

        public ServiceResponse<IEnumerable<CommonUser>> Select()
        {
            var response = new ServiceResponse<IEnumerable<CommonUser>>();

            response.Result = _repository.Select(x => x.Active);

            return response;
        }

        public ServiceResponse<Guid> Add(CommonUser commonUser)
        {
            var response = new ServiceResponse<Guid>();

            if (!commonUser.Validate())
                response.AddMessages(commonUser.Notifications);

            if (!response.Messages.Any())
            {
                response.Result = _repository.Insert(commonUser);
            }

            return response;
        }

        public ServiceResponse<CommonUser> Update(Guid id, CommonUser commonUser)
        {
            var response = new ServiceResponse<CommonUser>();
            commonUser.Id = id;

            if (!commonUser.Validate())
                response.AddMessages(commonUser.Notifications);

            var entity = _repository.Select(x => x.Id == id && x.Active).FirstOrDefault();

            if (entity == null)
                response.Messages.Add(ServiceMessages.ID_NOT_EXISTS_MESSAGE);

            if (!response.Messages.Any())
            {
                commonUser.CreateDate = entity.CreateDate;
                _repository.Update(commonUser);
                response.Result = _repository.Select(id);
            }

            return response;
        }

        public ServiceResponse<bool> Delete(Guid id)
        {
            var response = new ServiceResponse<bool>();

            var entity = _repository.Select(id);

            if (entity == null)
                response.Messages.Add(ServiceMessages.ID_NOT_EXISTS_MESSAGE);

            if (!response.Messages.Any())
            {
                entity.Deactivate();
                _repository.Update(entity);
            }

            return response;
        }

        public ServiceResponse<CommonUser> Login(string email, string password)
        {
            var response = new ServiceResponse<CommonUser>();

            var entity = _repository.Select(x => x.Email == email && x.Password == password).FirstOrDefault();

            if (entity == null)
                response.Messages.Add("Nenhum usuário encontrado");
            else
                response.Result = entity;

            return response;
        }
    }
}
