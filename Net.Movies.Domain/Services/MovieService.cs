﻿using Net.Movies.Domain.Comunication;
using Net.Movies.Domain.Contracts;
using Net.Movies.Domain.Entities;
using Net.Movies.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;

namespace Net.Movies.Domain.Services
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _repository;

        public MovieService(IMovieRepository repository)
        {
            _repository = repository;
        }

        public ServiceResponse<IEnumerable<Movie>> Select()
        {
            var response = new ServiceResponse<IEnumerable<Movie>>();

            response.Result = _repository.Select(x => x.Director);

            return response;
        }

        public ServiceResponse<Guid> Add(Movie movie)
        {
            var response = new ServiceResponse<Guid>();

            if (movie.Validate())
                response.AddMessages(movie.Notifications);

            if (!response.Messages.Any())
            {
                response.Result = _repository.Insert(movie);
            }

            return response;
        }

        public ServiceResponse<Movie> SelectFull(Guid id)
        {
            var response = new ServiceResponse<Movie>();

            var entity = _repository.Select(id, w => w.Director, x => x.MovieRatings);

            if (entity == null)
                response.Messages.Add(ServiceMessages.ID_NOT_EXISTS_MESSAGE);
            else
                response.Result = entity;

            return response;
        }
    }
}
