﻿using Net.Movies.Domain.Comunication;
using Net.Movies.Domain.Contracts;
using Net.Movies.Domain.Entities;
using Net.Movies.Domain.Repositories;
using System;
using System.Linq;

namespace Net.Movies.Domain.Services
{
    public class ActorService : IActorService
    {
        protected readonly IActorRepository _repository;

        public ActorService(IActorRepository repository)
        {
            _repository = repository;
        }

        public ServiceResponse<Guid> Add(Actor actor)
        {
            var response = new ServiceResponse<Guid>();

            if (!actor.Validate())
                response.AddMessages(actor.Notifications);

            if (!response.Messages.Any())
            {
                response.Result = _repository.Insert(actor);
            }

            return response;
        }
    }
}
