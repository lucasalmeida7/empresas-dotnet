﻿
using Net.Movies.Domain.Comunication;
using Net.Movies.Domain.Contracts;
using Net.Movies.Domain.Entities;
using Net.Movies.Domain.Repositories;
using System;
using System.Linq;

namespace Net.Movies.Domain.Services
{
    public class DirectorService : IDirectorService
    {
        protected readonly IDirectorRepository _repository;

        public DirectorService(IDirectorRepository repository)
        {
            _repository = repository;
        }

        public ServiceResponse<Guid> Add(Director director)
        {
            var response = new ServiceResponse<Guid>();

            if (!director.Validate())
                response.AddMessages(director.Notifications);

            if (!response.Messages.Any())
            {
                response.Result = _repository.Insert(director);
            }

            return response;
        }
    }
}
