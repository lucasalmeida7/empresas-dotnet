﻿
using Net.Movies.Domain.Comunication;
using Net.Movies.Domain.Contracts;
using Net.Movies.Domain.Entities;
using Net.Movies.Domain.Repositories;
using System;
using System.Linq;

namespace Net.Movies.Domain.Services
{
    public class MovieActorService : IMovieActorService
    {
        private readonly IMovieActorRepository _repository;

        public MovieActorService(IMovieActorRepository repository)
        {
            _repository = repository;
        }

        public ServiceResponse<Guid> Add(MovieActor movieActor)
        {
            var response = new ServiceResponse<Guid>();

            if (movieActor.Validate())
                response.AddMessages(movieActor.Notifications);

            if (!response.Messages.Any())
            {
                response.Result = _repository.Insert(movieActor);
            }

            return response;
        }
    }
}
