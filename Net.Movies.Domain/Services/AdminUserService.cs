﻿
using Net.Movies.Domain.Comunication;
using Net.Movies.Domain.Contracts;
using Net.Movies.Domain.Entities;
using Net.Movies.Domain.Repositories;
using System;
using System.Linq;

namespace Net.Movies.Domain.Services
{
    public class AdminUserService : IAdminUserService
    {
        private readonly IAdminUserRepository _repository;

        public AdminUserService(IAdminUserRepository repository)
        {
            _repository = repository;
        }

        public ServiceResponse<Guid> Add(AdminUser adminUser)
        {
            var response = new ServiceResponse<Guid>();

            if (!adminUser.Validate())
                response.AddMessages(adminUser.Notifications);

            if (!response.Messages.Any())
            {
                response.Result = _repository.Insert(adminUser);
            }

            return response;
        }

        public ServiceResponse<AdminUser> Update(Guid id, AdminUser adminUser)
        {
            var response = new ServiceResponse<AdminUser>();
            adminUser.Id = id;

            if (!adminUser.Validate())
                response.AddMessages(adminUser.Notifications);

            var entity = _repository.Select(x => x.Id == id && x.Active).FirstOrDefault();

            if (entity == null)
                response.Messages.Add(ServiceMessages.ID_NOT_EXISTS_MESSAGE);

            if (!response.Messages.Any())
            {
                _repository.Update(adminUser);
                response.Result = adminUser;
            }

            return response;
        }

        public ServiceResponse<bool> Delete(Guid id)
        {
            var response = new ServiceResponse<bool>();

            var entity = _repository.Select(id);

            if (entity == null)
                response.Messages.Add(ServiceMessages.ID_NOT_EXISTS_MESSAGE);

            if (!response.Messages.Any())
            {
                entity.Deactivate();
                _repository.Update(entity);
                response.Result = true;
            }

            return response;
        }

        public ServiceResponse<AdminUser> Login(string email, string password)
        {
            var response = new ServiceResponse<AdminUser>();

            var entity = _repository.Select(x => x.Email == email && x.Password == password).FirstOrDefault();

            if (entity == null)
                response.Messages.Add("Nenhum usuário encontrado");
            else
                response.Result = entity;

            return response;
        }
    }
}
