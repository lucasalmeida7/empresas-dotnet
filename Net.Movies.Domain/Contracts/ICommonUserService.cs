﻿using Net.Movies.Domain.Comunication;
using Net.Movies.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Net.Movies.Domain.Contracts
{
    public interface ICommonUserService
    {
        ServiceResponse<Guid> Add(CommonUser commonUser);
        ServiceResponse<bool> Delete(Guid id);
        ServiceResponse<CommonUser> Login(string email, string password);
        ServiceResponse<IEnumerable<CommonUser>> Select();
        ServiceResponse<CommonUser> Update(Guid id, CommonUser commonUser);
    }
}
