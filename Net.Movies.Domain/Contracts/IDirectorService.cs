﻿
using Net.Movies.Domain.Comunication;
using Net.Movies.Domain.Entities;
using System;

namespace Net.Movies.Domain.Contracts
{
    public interface IDirectorService
    {
        ServiceResponse<Guid> Add(Director director);
    }
}
