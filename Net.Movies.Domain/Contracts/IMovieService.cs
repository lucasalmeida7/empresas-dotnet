﻿
using Net.Movies.Domain.Comunication;
using Net.Movies.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Net.Movies.Domain.Contracts
{
    public interface IMovieService
    {
        ServiceResponse<Guid> Add(Movie movie);
        ServiceResponse<IEnumerable<Movie>> Select();
        ServiceResponse<Movie> SelectFull(Guid id);
    }
}
