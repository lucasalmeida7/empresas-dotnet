﻿
using Net.Movies.Domain.Comunication;
using Net.Movies.Domain.Entities;
using System;

namespace Net.Movies.Domain.Contracts
{
    public interface IGenderService
    {
        ServiceResponse<Guid> Add(Gender gender);
    }
}
