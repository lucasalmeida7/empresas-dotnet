﻿
using Net.Movies.Domain.Comunication;
using Net.Movies.Domain.Entities;
using System;

namespace Net.Movies.Domain.Contracts
{
    public interface IMovieActorService
    {
        ServiceResponse<Guid> Add(MovieActor movieActor);
    }
}
