﻿
using Net.Movies.Domain.Comunication;
using Net.Movies.Domain.Entities;
using System;

namespace Net.Movies.Domain.Contracts
{
    public interface IAdminUserService
    {
        ServiceResponse<Guid> Add(AdminUser adminUser);
        ServiceResponse<bool> Delete(Guid id);
        ServiceResponse<AdminUser> Login(string email, string password);
        ServiceResponse<AdminUser> Update(Guid id, AdminUser adminUser);
    }
}
