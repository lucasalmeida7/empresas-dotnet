﻿
using Net.Movies.Domain.Comunication;
using Net.Movies.Domain.Entities;
using System;

namespace Net.Movies.Domain.Contracts
{
    public interface IMovieGenderService
    {
        ServiceResponse<Guid> Add(MovieGender movieGender);
    }
}
