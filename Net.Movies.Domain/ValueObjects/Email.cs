﻿
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Net.Movies.Domain.ValueObjects
{
    public class Email : ValueObject
    {
        public Email(string address)
        {
            Address = address;

            if (string.IsNullOrWhiteSpace(Address))
                AddNotification("O endereço deve ser informado");

            if (Address.Length > 50)
                AddNotification("O endereço não deve possuir tamanho maior que 50");

            if(!Regex.IsMatch(address, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"))
                AddNotification("Formato de e-mail inválido");
        }

        public string Address { get; private set; }
    }
}
