﻿using Net.Movies.Domain.Entities;
using Net.Movies.Domain.Repositories;
using Net.Movies.Infra.Data.Context;

namespace Net.Movies.Infra.Data.Repositories
{
    public class MovieActorRepository : Repository<MovieActor>, IMovieActorRepository
    {
        public MovieActorRepository(NetMoviesContext context)
            : base(context)
        {
        }
    }
}
