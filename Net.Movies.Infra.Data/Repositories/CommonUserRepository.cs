﻿using Net.Movies.Domain.Entities;
using Net.Movies.Domain.Repositories;
using Net.Movies.Infra.Data.Context;

namespace Net.Movies.Infra.Data.Repositories
{
    public class CommonUserRepository : Repository<CommonUser>, ICommonUserRepository
    {
        public CommonUserRepository(NetMoviesContext context)
            : base(context)
        {
        }
    }
}
