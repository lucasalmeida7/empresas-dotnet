﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Net.Movies.Domain.Entities;
using Net.Movies.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Net.Movies.Infra.Data.Repositories
{
    public class Repository<T> : IRepository<T> where T : Entity
    {
        protected readonly DbContext _context;
        protected DbSet<T> _set;
        private IDbContextTransaction transaction;

        protected Repository(DbContext context)
        {
            _context = context;
            _set = context.Set<T>();
        }

        public virtual IQueryable<T> Select()
        {
            return _set.AsNoTracking();
        }

        public virtual IQueryable<T> Select(params Expression<Func<T, object>>[] includes)
        {
            var query = _set.AsQueryable();

            if (includes != null || includes.Count() > 0)
                query = includes.Aggregate(query, (current, expression) => current.Include(expression));

            return query;
        }

        public virtual IQueryable<T> Select(Func<T, bool> predicated)
        {
            return _set.AsNoTracking().Where(predicated).AsQueryable();
        }

        public virtual IQueryable<T> Select(Func<T, bool> predicate, params Expression<Func<T, object>>[] includes)
        {
            var query = _set.AsQueryable();

            if (includes != null || includes.Count() > 0)
                query = includes.Aggregate(query, (current, include) => current.Include(include));

            return query.Where(predicate).AsQueryable();
        }


        public virtual T Select(Guid id, params Expression<Func<T, object>>[] includes)
        {
            var query = _set.AsQueryable();

            if (includes != null || includes.Count() > 0)
                query = includes.Aggregate(query, (current, include) => current.Include(include));

            return query.FirstOrDefault(x => x.Id == id);
        }

        public virtual T Select(Guid id)
        {
            return _set.Find(id);
        }


        public virtual Guid Insert(T entity)
        {
            transaction = _context.Database.BeginTransaction();

            try
            {
                _set.Add(entity);

                _context.SaveChanges();
                transaction.Commit();

                return entity.Id;
            }
            catch (Exception)
            {
                transaction.Rollback();

                throw;
            }


        }

        public virtual void Update(T entity)
        {
            transaction = _context.Database.BeginTransaction();

            try
            {
                _set.Update(entity);

                _context.SaveChanges();
                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();

                throw;
            }
        }

        public virtual void Delete(Guid id)
        {
            transaction = _context.Database.BeginTransaction();

            try
            {
                var entity = Select(id);

                _set.Remove(entity);

                _context.SaveChanges();
                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();

                throw;
            }
        }

        public virtual void Delete(IEnumerable<T> entities)
        {
            transaction = _context.Database.BeginTransaction();

            try
            {
                _set.RemoveRange(entities);

                _context.SaveChanges();
                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();

                throw;
            }
        }
    }
}
