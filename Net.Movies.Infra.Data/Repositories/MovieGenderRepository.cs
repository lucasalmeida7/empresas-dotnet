﻿
using Net.Movies.Domain.Entities;
using Net.Movies.Domain.Repositories;
using Net.Movies.Infra.Data.Context;

namespace Net.Movies.Infra.Data.Repositories
{
    public class MovieGenderRepository : Repository<MovieGender>, IMovieGenderRepository
    {
        public MovieGenderRepository(NetMoviesContext context)
            : base(context)
        {
        }
    }
}
