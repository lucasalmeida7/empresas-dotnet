﻿using Net.Movies.Domain.Entities;
using Net.Movies.Domain.Repositories;
using Net.Movies.Infra.Data.Context;

namespace Net.Movies.Infra.Data.Repositories
{
    public class DirectorRepository : Repository<Director>, IDirectorRepository
    {
        public DirectorRepository(NetMoviesContext context)
            : base(context)
        {
        }
    }
}
