﻿using Microsoft.EntityFrameworkCore;
using Net.Movies.Domain.Entities;
using Net.Movies.Domain.Repositories;
using Net.Movies.Infra.Data.Context;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Net.Movies.Infra.Data.Repositories
{
    public class MovieRepository : Repository<Movie>, IMovieRepository
    {
        public MovieRepository(NetMoviesContext context)
            : base(context)
        {
        }

        public override Movie Select(Guid id, params Expression<Func<Movie, object>>[] includes)
        {
            var query = _set
                .Include(x => x.MovieActors)
                    .ThenInclude(y => y.Actor)
                .Include(x => x.MovieGenders)
                    .ThenInclude(y => y.Gender)
                .AsQueryable();

            if (includes != null || includes.Count() > 0)
                query = includes.Aggregate(query, (current, include) => current.Include(include));

            return query.FirstOrDefault(x => x.Id == id);
        }
    }
}
