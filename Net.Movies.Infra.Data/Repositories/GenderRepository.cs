﻿using Net.Movies.Domain.Entities;
using Net.Movies.Domain.Repositories;
using Net.Movies.Infra.Data.Context;

namespace Net.Movies.Infra.Data.Repositories
{
    public class GenderRepository : Repository<Gender>, IGenderRepository
    {
        public GenderRepository(NetMoviesContext context)
            : base(context)
        {
        }
    }
}
