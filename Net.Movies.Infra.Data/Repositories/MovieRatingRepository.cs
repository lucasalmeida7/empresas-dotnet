﻿using Net.Movies.Domain.Entities;
using Net.Movies.Domain.Repositories;
using Net.Movies.Infra.Data.Context;

namespace Net.Movies.Infra.Data.Repositories
{
    public class MovieRatingRepository : Repository<MovieRating>, IMovieRatingRepository
    {
        public MovieRatingRepository(NetMoviesContext context)
            : base(context)
        {
        }
    }
}
