﻿
using Microsoft.EntityFrameworkCore;
using Net.Movies.Domain.Entities;
using Net.Movies.Infra.Data.Mappings;

namespace Net.Movies.Infra.Data.Context
{
    public class NetMoviesContext : DbContext
    {
        public NetMoviesContext(DbContextOptions<NetMoviesContext> options) : base(options)
        {
        }

        #region DbSets

        public DbSet<AdminUser> AdminUsers { get; set; }
        public DbSet<CommonUser> CommonUsers { get; set; }

        public DbSet<Director> Directors { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<Actor> Actors { get; set; }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<MovieGender> MovieGenders { get; set; }
        public DbSet<MovieActor> MovieActors { get; set; }
        public DbSet<MovieRating> MovieRatings { get; set; }

        #endregion

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new AdminUserMap());
            builder.ApplyConfiguration(new CommonUserMap());

            builder.ApplyConfiguration(new DirectorMap());
            builder.ApplyConfiguration(new GenderMap());
            builder.ApplyConfiguration(new ActorMap());

            builder.ApplyConfiguration(new MovieMap());
            builder.ApplyConfiguration(new MovieGenderMap());
            builder.ApplyConfiguration(new MovieActorMap());
            builder.ApplyConfiguration(new MovieRatingMap());

        }
    }
}
