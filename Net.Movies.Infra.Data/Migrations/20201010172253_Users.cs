﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Net.Movies.Infra.Data.Migrations
{
    public partial class Users : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ADMIN_USER",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    EMAIL = table.Column<string>(maxLength: 50, nullable: true),
                    PASSWORD = table.Column<string>(maxLength: 20, nullable: false),
                    ACTIVE = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ADMIN_USER", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "COMMON_USER",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    EMAIL = table.Column<string>(maxLength: 50, nullable: true),
                    PASSWORD = table.Column<string>(maxLength: 20, nullable: false),
                    ACTIVE = table.Column<bool>(nullable: false),
                    CREATE_DATE = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_COMMON_USER", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ADMIN_USER");

            migrationBuilder.DropTable(
                name: "COMMON_USER");
        }
    }
}
