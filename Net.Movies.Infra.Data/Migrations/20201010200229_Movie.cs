﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Net.Movies.Infra.Data.Migrations
{
    public partial class Movie : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MOVIE",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    TITLE = table.Column<string>(maxLength: 100, nullable: false),
                    YEAR = table.Column<int>(maxLength: 4, nullable: false),
                    DIRECTOR_ID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MOVIE", x => x.ID);
                    table.ForeignKey(
                        name: "FK_MOVIE_DIRECTOR_DIRECTOR_ID",
                        column: x => x.DIRECTOR_ID,
                        principalTable: "DIRECTOR",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MOVIE_ACTOR",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    MOVIE_ID = table.Column<Guid>(nullable: false),
                    ACTOR_ID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MOVIE_ACTOR", x => x.ID);
                    table.ForeignKey(
                        name: "FK_MOVIE_ACTOR_ACTOR_ACTOR_ID",
                        column: x => x.ACTOR_ID,
                        principalTable: "ACTOR",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MOVIE_ACTOR_MOVIE_MOVIE_ID",
                        column: x => x.MOVIE_ID,
                        principalTable: "MOVIE",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MOVIE_GENDER",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    MOVIE_ID = table.Column<Guid>(nullable: false),
                    GENDER_ID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MOVIE_GENDER", x => x.ID);
                    table.ForeignKey(
                        name: "FK_MOVIE_GENDER_GENDER_GENDER_ID",
                        column: x => x.GENDER_ID,
                        principalTable: "GENDER",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MOVIE_GENDER_MOVIE_MOVIE_ID",
                        column: x => x.MOVIE_ID,
                        principalTable: "MOVIE",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MOVIE_RATING",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    MOVIE_ID = table.Column<Guid>(nullable: false),
                    ACTOR_ID = table.Column<Guid>(nullable: false),
                    SCORE = table.Column<int>(maxLength: 1, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MOVIE_RATING", x => x.ID);
                    table.ForeignKey(
                        name: "FK_MOVIE_RATING_COMMON_USER_ACTOR_ID",
                        column: x => x.ACTOR_ID,
                        principalTable: "COMMON_USER",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MOVIE_RATING_MOVIE_MOVIE_ID",
                        column: x => x.MOVIE_ID,
                        principalTable: "MOVIE",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MOVIE_DIRECTOR_ID",
                table: "MOVIE",
                column: "DIRECTOR_ID");

            migrationBuilder.CreateIndex(
                name: "IX_MOVIE_ACTOR_ACTOR_ID",
                table: "MOVIE_ACTOR",
                column: "ACTOR_ID");

            migrationBuilder.CreateIndex(
                name: "IX_MOVIE_ACTOR_MOVIE_ID",
                table: "MOVIE_ACTOR",
                column: "MOVIE_ID");

            migrationBuilder.CreateIndex(
                name: "IX_MOVIE_GENDER_GENDER_ID",
                table: "MOVIE_GENDER",
                column: "GENDER_ID");

            migrationBuilder.CreateIndex(
                name: "IX_MOVIE_GENDER_MOVIE_ID",
                table: "MOVIE_GENDER",
                column: "MOVIE_ID");

            migrationBuilder.CreateIndex(
                name: "IX_MOVIE_RATING_ACTOR_ID",
                table: "MOVIE_RATING",
                column: "ACTOR_ID");

            migrationBuilder.CreateIndex(
                name: "IX_MOVIE_RATING_MOVIE_ID",
                table: "MOVIE_RATING",
                column: "MOVIE_ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MOVIE_ACTOR");

            migrationBuilder.DropTable(
                name: "MOVIE_GENDER");

            migrationBuilder.DropTable(
                name: "MOVIE_RATING");

            migrationBuilder.DropTable(
                name: "MOVIE");
        }
    }
}
