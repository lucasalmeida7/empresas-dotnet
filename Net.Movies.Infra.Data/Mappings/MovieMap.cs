﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Net.Movies.Domain.Entities;

namespace Net.Movies.Infra.Data.Mappings
{
    public class MovieMap : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            builder.ToTable("MOVIE");
            builder.HasKey(x => x.Id);

            builder.Property(c => c.Id).HasColumnName("ID").IsRequired();
            builder.Property(c => c.Title).HasColumnName("TITLE").IsRequired().HasMaxLength(100);
            builder.Property(c => c.Year).HasColumnName("YEAR").IsRequired().HasMaxLength(4);
            builder.Property(c => c.DirectorId).HasColumnName("DIRECTOR_ID").IsRequired();

            builder.HasOne(m => m.Director)
                .WithMany(d => d.Movies);
        }
    }
}
