﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Net.Movies.Domain.Entities;

namespace Net.Movies.Infra.Data.Mappings
{
    public class MovieActorMap : IEntityTypeConfiguration<MovieActor>
    {
        public void Configure(EntityTypeBuilder<MovieActor> builder)
        {
            builder.ToTable("MOVIE_ACTOR");
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Movie)
                .WithMany(v => v.MovieActors)
                .HasForeignKey(x => x.MovieId);

            builder.HasOne(x => x.Actor)
                .WithMany(v => v.MovieActors)
                .HasForeignKey(x => x.ActorId);

            builder.Property(x => x.Id).HasColumnName("ID").IsRequired();
            builder.Property(x => x.MovieId).HasColumnName("MOVIE_ID").IsRequired();
            builder.Property(x => x.ActorId).HasColumnName("ACTOR_ID").IsRequired();
        }
    }
}
