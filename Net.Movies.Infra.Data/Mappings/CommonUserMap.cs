﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Net.Movies.Domain.Entities;
using System.Linq;

namespace Net.Movies.Infra.Data.Mappings
{
    public class CommonUserMap : IEntityTypeConfiguration<CommonUser>
    {
        public void Configure(EntityTypeBuilder<CommonUser> builder)
        {
            builder.ToTable("COMMON_USER");
            builder.HasKey(x => x.Id);

            builder.Property(c => c.Id).HasColumnName("ID").IsRequired();
            builder.Property(c => c.Name).HasColumnName("NAME").IsRequired().HasMaxLength(50);
            builder.Property(x => x.Email).HasColumnName("EMAIL").IsRequired().HasMaxLength(50);
            builder.Property(c => c.Password).HasColumnName("PASSWORD").IsRequired().HasMaxLength(20);
            builder.Property(c => c.Active).HasColumnName("ACTIVE").IsRequired();
            builder.Property(c => c.CreateDate).HasColumnName("CREATE_DATE").IsRequired();
        }
    }
}

