﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Net.Movies.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Net.Movies.Infra.Data.Mappings
{
    public class MovieRatingMap : IEntityTypeConfiguration<MovieRating>
    {
        public void Configure(EntityTypeBuilder<MovieRating> builder)
        {
            builder.ToTable("MOVIE_RATING");
            builder.HasKey(x => x.Id);

            builder.HasOne(m => m.Movie)
                .WithMany(d => d.MovieRatings);

            builder.HasOne(m => m.CommonUser)
                .WithMany(d => d.MovieRatings);

            builder.Property(x => x.Id).HasColumnName("ID").IsRequired();
            builder.Property(x => x.MovieId).HasColumnName("MOVIE_ID").IsRequired();
            builder.Property(x => x.CommonUserId).HasColumnName("ACTOR_ID").IsRequired();
            builder.Property(x => x.Score).HasColumnName("SCORE").IsRequired().HasMaxLength(1);
        }
    }
}
