﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Net.Movies.Domain.Entities;

namespace Net.Movies.Infra.Data.Mappings
{
    public class AdminUserMap : IEntityTypeConfiguration<AdminUser>
    {
        public void Configure(EntityTypeBuilder<AdminUser> builder)
        {
            builder.ToTable("ADMIN_USER");
            builder.HasKey(x => x.Id);

            builder.Property(c => c.Id).HasColumnName("ID").IsRequired();
            builder.Property(c => c.Name).HasColumnName("NAME").IsRequired().HasMaxLength(50);
            builder.Property(x => x.Email).HasColumnName("EMAIL").IsRequired().HasMaxLength(50);
            builder.Property(c => c.Password).HasColumnName("PASSWORD").IsRequired().HasMaxLength(20);
            builder.Property(c => c.Active).HasColumnName("ACTIVE").IsRequired();
        }
    }
}
