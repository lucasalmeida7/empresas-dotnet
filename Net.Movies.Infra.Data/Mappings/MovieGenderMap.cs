﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Net.Movies.Domain.Entities;

namespace Net.Movies.Infra.Data.Mappings
{
    public class MovieGenderMap : IEntityTypeConfiguration<MovieGender>
    {
        public void Configure(EntityTypeBuilder<MovieGender> builder)
        {
            builder.ToTable("MOVIE_GENDER");
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Movie)
                .WithMany(v => v.MovieGenders)
                .HasForeignKey(x => x.MovieId);

            builder.HasOne(x => x.Gender)
                .WithMany(v => v.MovieGenders)
                .HasForeignKey(x => x.GenderId);

            builder.Property(x => x.Id).HasColumnName("ID").IsRequired();
            builder.Property(x => x.MovieId).HasColumnName("MOVIE_ID").IsRequired();
            builder.Property(x => x.GenderId).HasColumnName("GENDER_ID");
        }
    }
}
