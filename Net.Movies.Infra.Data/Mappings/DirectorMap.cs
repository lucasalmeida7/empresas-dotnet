﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Net.Movies.Domain.Entities;

namespace Net.Movies.Infra.Data.Mappings
{
    public class DirectorMap : IEntityTypeConfiguration<Director>
    {
        public void Configure(EntityTypeBuilder<Director> builder)
        {
            builder.ToTable("DIRECTOR");
            builder.HasKey(x => x.Id);

            builder.Property(c => c.Id).HasColumnName("ID").IsRequired();
            builder.Property(c => c.Name).HasColumnName("NAME").IsRequired().HasMaxLength(50);
        }
    }
}